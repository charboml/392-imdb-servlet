var app = angular.module ("imdb");

app.controller('MovieSearchCtrl', function($scope, $http) {    
    
	$scope.year = -1;
    $scope.title = "";
    $scope.runningTimeComparator = "<=";
    $scope.imdbRating="";
    $scope.movies = [];
    $scope.searchPerformed = false;
	$scope.genre1 = "null";
	$scope.genre2 = "null";
	$scope.genre3 = "null";
	$scope.mpaaRating = "null";
	
	$scope.person1Name = "";
	$scope.person1Role = "null";
	
	$scope.person2Name = "";
	$scope.person2Role = "null";
	
	$scope.person3Name = "";
	$scope.person3Role = "null";
	     
    $scope.searchDisabled = function () {
		if ($scope.person1Name.length > 0 && $scope.person1Role == "null") {
			return true;
		}

		if ($scope.person2Name.length > 0 && $scope.person2Role == "null") {
			return true;
		}

		if ($scope.person3Name.length > 0 && $scope.person3Role == "null") {
			return true;
		}
						
    	return $scope.title.length == 0 && $scope.year == -1 && 
			(!$scope.runningTime || $scope.runningTime.length == 0) && 
			$scope.genre1 == "null" && $scope.genre2 == "null" && $scope.genre3 == "null" && 
			$scope.person1Name.length == 0 && $scope.person2Name.length == 0 && $scope.person3Name.length == 0
			;
    }
    
    $scope.searchForMovies = function () {
    	console.log("IMDB Rating");		
    	
    	var searchParameters = {
    		title: "",    		
    		year: $scope.year,
    		runningTimeComparator: $scope.runningTimeComparator,
    		runningTime: -1,
			imdbRating: -1,
			people: [],
			categories: []			
    	};    			
		
    	if ($scope.title.length > 0) {
    		searchParameters.title = $scope.title;
    	}
    	
    	if ($scope.runningTime && $scope.runningTime.length > 0) {
    		searchParameters.runningTime = $scope.runningTime;
    	}

		if ($scope.imdbRating && $scope.imdbRating > 0 && $scope.imdbRating <= 10) {
			searchParameters.imdbRating = $scope.imdbRating;
		}
		
		if ($scope.mpaaRating != "null") {
			searchParameters.mpaaRating = $scope.mpaaRating;
		}

		var genres = [];
		if ($scope.genre1 != "null") {
			genres.push($scope.genre1);
		}
		
		if ($scope.genre2 != "null") {
			genres.push($scope.genre2);
		}
		
		if ($scope.genre3 != "null") {
			genres.push($scope.genre3);
		}
		
		if ($scope.person1Name.length > 0) {
			searchParameters.people.push($scope.person1Name);
			searchParameters.categories.push($scope.person1Role);	
		}

		if ($scope.person2Name.length > 0) {
			searchParameters.people.push($scope.person2Name);
			searchParameters.categories.push($scope.person2Role);	
		}
		
		if ($scope.person3Name.length > 0) {
			searchParameters.people.push($scope.person3Name);
			searchParameters.categories.push($scope.person3Role);	
		}
				
		searchParameters.genres = genres;
		
		console.log(searchParameters);
		    	
    	$http.post("api/movies/search", searchParameters).then(
    			function (response) {
    				$scope.searchPerformed = true;
    				$scope.movies = response.data;
					$scope.movies.forEach( movie => movie.genres = movie.genres.join(", ")); 															
    			}
    		);
    };
        
    $http.get("api/genres").then(
    	function (response) {
    		$scope.genres = response.data;
    	}
    );


	$http.get("api/MPAARatings").then(r => $scope.mpaaRatings = r.data);    
	$http.get("api/categories").then(r => $scope.categories = r.data);    
	$http.get("api/movies/years").then(r => {$scope.years = r.data; console.log($scope.years);});		
});