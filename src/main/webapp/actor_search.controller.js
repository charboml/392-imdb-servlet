var app = angular.module ("imdb");

app.controller('ActorSearchCtrl', function($scope, $http) {
	    		
    $scope.firstName = "";
    $scope.lastName = "";
    $scope.birthYear = "";
    $scope.deathYear = "";
    
    $scope.actors = [];
    $scope.searchPerformed = false;
    
    $scope.selectedActor = undefined;
    
    $scope.showMoviesForActor = function (actor) {
    	$http.get("api/actors/" + actor.personID + "/roles").then(
    		function (response) {
    			$scope.selectedActor = actor;
    			$scope.selectedActor.roles = response.data;
    		}
    	);    	    	
    };
    
    $scope.searchDisabled = function () {
    	return $scope.firstName.length == 0 && $scope.lastName.length == 0 && 
    	       $scope.birthYear.length == 0 && $scope.deathYear.length == 0;
    }
    
    $scope.searchForActors = function () {        	
    	var searchParameters = {
    		firstName: "",
    		lastName: "",
    		birthYear: 0,
    		deathYear: 0
    	};
    	
    	if ($scope.firstName.length > 0) {
    		searchParameters.firstName = $scope.firstName;
    	}
    	
       	if ($scope.lastName.length > 0) {
    		searchParameters.lastName = $scope.lastName;
    	}
    	
       	if ($scope.birthYear > 0) {
    		searchParameters.birthYear = $scope.birthYear;
    	}

       	if ($scope.deathYear > 0) {
    		searchParameters.deathYear = $scope.deathYear;
    	}
    	              	
    	$http.post("api/actors/search", searchParameters).then(
			function (response) {
				$scope.searchPerformed = true;
				$scope.actors = response.data;
			}
		);
    };
});