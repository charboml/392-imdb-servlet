package edu.hope.cs.csci392.imdb;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.hope.cs.csci392.imdb.model.Actor;
import edu.hope.cs.csci392.imdb.model.Movie;
import edu.hope.cs.csci392.imdb.model.Role;

public class Database {
	private static Database instance;
	
	private Database ()
	{			
	}
	
	public static Database getInstance () {
		if (instance == null) {
			instance = new Database ();
		}
		
		return instance;
	}
	
	/**
	 * Finds the actors that match the given conditions.  Only those 
	 * parameters that have non-empty values are included in the query.  
	 * Those parameters that are included are combined using AND.
	 * @param firstName the first name of the actor(s) to locate
	 * @param lastName the last name of the actor(s) to locate
	 * @param birthYear the year when the actor(s) to locate were born
	 * @param deathYear the year when the actor(s) to locate died
	 * @return a List of Actor objects representing the actor(s) matching the
	 * specified conditions
	 * @throws SQLException 
	 */
	public List<Actor> findActors (
		String firstName, String lastName,
		int birthYear, int deathYear
	) throws SQLException
	{
		// Start an SQL statement
		StringBuffer sql = new StringBuffer (1024);
		String connector = " where ";
		sql.append("select * from imdb.People ");
		// Build up the WHERE clause based on search parameters
		if (firstName != null && firstName.length() > 0) {
			sql.append(connector);
			sql.append("FirstName = '");
			sql.append(firstName);
			sql.append("'");
			connector = " and ";
		}
		if (lastName != null && lastName.length() > 0) {
			sql.append(connector);
			sql.append("LastName = '");
			sql.append(lastName);
			sql.append("'");
		}
		if (birthYear > 0) {
			sql.append(connector);
			sql.append("birthYear = '");
			sql.append(birthYear);
			sql.append("'");
		}
		if (deathYear > 0 && birthYear > deathYear) {
			sql.append(connector);
			sql.append("deathYear = '");
			sql.append(deathYear);
			sql.append("'");
		}

		sql.append(" order by LastName, FirstName");
		try (Connection conn = ConnectionFactory.getConnection()){	
				ArrayList<Actor> actors = new ArrayList<Actor> ();
				java.sql.PreparedStatement statement = conn.prepareStatement(sql.toString());
				ResultSet results = statement.executeQuery(sql.toString());
				while (results.next()) {
					// Process one row of results
					String personID = results.getString("PersonID").trim();
						firstName = results.getString("FirstName");
					String middleName = results.getString("MiddleName");
						lastName = results.getString("LastName");
					String suffix = results.getString("Suffix");
					String fullName = results.getString("FullName");
						birthYear = results.getInt("BirthYear");
						deathYear = results.getInt("DeathYear");
					Actor actor = new Actor(
					personID, firstName, middleName, lastName, suffix, fullName,
					birthYear, deathYear
					);
					actors.add(actor);
					}
				return actors;
				}
				catch (SQLException ex) { return null; }
	}
	
	/**
	 * Finds the movies that match the given conditions.  All conditions
	 * are combined using AND.
	 * @param movieTitle the title of the movie(s) to be located, or an
	 * empty string if title should not be included as part of the criteria
	 * @param year the year the movie was made, or -1 if year should not be
	 * included as part of the criteria
	 * @param runningTimeComparator a string indicating the type of comparison
	 * to be used for the running time.  This string can be used directly in
	 * the SQL query
	 * @param runningTimeValue the running time of the movie(s) to be located,
	 * or -1 if running time should not be included as part of the criteria
	 * @param primaryGenre the primary genre of the movie(s) to be located,
	 * or an empty string if the primary genre should not be included as part
	 * of the criteria
	 * @param director the director of the movie(s) to be located, or an empty string
	 * if the director should not be included as part of the criteria
	 * @return a List of Movie objects representing the movie(s) matching the
	 * specified conditions
	 */
	public List<Movie> findMovies (
		String movieTitle, int year, String runningTimeComparator, 
		int runningTimeValue, String mpaaRating, float minimumIMDBRating,
		List<String> genres, List<String> people, List<String> categories
	) throws SQLException {
		ArrayList<Movie> movies = new ArrayList<Movie> ();
		// Start an SQL statement
				StringBuffer sql = new StringBuffer (1024);
				String connector = " where ";
				sql.append("select * from imdb.Movies");

				// Build up the WHERE clause based on search parameters
				if (movieTitle != null && movieTitle.length() > 0) {
					sql.append(connector);
					sql.append("title = '");
					sql.append(movieTitle);
					sql.append("'");
					connector = " and ";
				} 
				if (genres.size() > 0) {
					int i = 0;
					while (genres.size() > i) {
						sql.append(connector);
						sql.append("exists ( select Genre from imdb.movieGenres where Genre = '");
						sql.append(genres.get(i));
						sql.append("' and movies.titleId = movieGenres.TitleID)");
						connector = " and ";
						i++;
					}
				}
				
				if (people.size() > 0 && categories.size() > 0) {
					int j = 0;
					while (people.size() > j) {
						sql.append(connector);
						sql.append("exists (select People.personID from imdb.principals join imdb.People on people.personID = principals.personID where fullName = '");
						sql.append(people.get(j));
						sql.append("'");
						sql.append("and Category = '");
						sql.append(categories.get(j));
						sql.append("'");
						sql.append(" and movies.titleId = principals.TitleID)");
						connector = " and ";
						j++;
					}
				}
				if (year > 0) {
					sql.append(connector);
					sql.append("yearReleased = '");
					sql.append(year);
					sql.append("'");
					connector = " and ";

				}
				if (runningTimeValue > 0) {
					sql.append(connector);
					sql.append("RunningTimeInMinutes ");
					sql.append(runningTimeComparator);
					sql.append(" '");
					sql.append(runningTimeValue);
					sql.append("'");
					connector = " and ";

				}
				if (mpaaRating != null && mpaaRating.length() > 0) {
					sql.append(connector);
					sql.append("MPAARating  = '");
					sql.append(mpaaRating);
					sql.append("'");
					connector = " and ";

				}
				if (minimumIMDBRating > 0) {
					sql.append(connector);
					sql.append("IMDBRating > ");
					sql.append(minimumIMDBRating);
					sql.append("'");
					connector = " and ";

				}
				try (Connection conn = ConnectionFactory.getConnection()){	
					java.sql.Statement statement = conn.createStatement();
					ResultSet results = statement.executeQuery(sql.toString());
					System.out.println(sql.toString());
					while (results.next()) {
						// Process one row of results
						String titleID = results.getString("TitleID").trim();
						movieTitle = results.getString("Title").trim();
							year = results.getInt("YearReleased");
							runningTimeValue = results.getInt("RunningTimeInMinutes");
						mpaaRating = results.getString("MPAARating");
						float imdbRating = results.getFloat("IMDBRating");
						int numberOfRatings = results.getInt("IMDBNumberRatings");
						Movie movie = new Movie(
						titleID, movieTitle, year, runningTimeValue, genres, mpaaRating, imdbRating, numberOfRatings
						);
						movies.add(movie);
						}
					for (int i = 0; i < movies.size(); i++) {
						 sql = new StringBuffer (1024);
						 connector = " where ";
						sql.append("select * from imdb.Movies join imdb.MovieGenres on Movies.titleID = MovieGenres.titleID ");
						sql.append(connector);
						sql.append(" Movies.titleID = ");
						sql.append("'");
						sql.append(movies.get(i).getMovieID());
						sql.append("' ");
						try (Connection conn2 = ConnectionFactory.getConnection()){	
							java.sql.Statement statement2 = conn.createStatement();
							ResultSet results2 = statement2.executeQuery(sql.toString());
							while (results2.next()) {
								movies.get(i).addGenre(results2.getString("Genre"));
							}
						}
					}
					return movies;
					}
	}
	
	/**
	 * Finds all the roles played by an actor.
	 * @param actor the actor to search for
	 * @return a List of Role objects representing the roles played by the
	 * specified actor
	 */
	public List<Role> findMoviesForActor (Actor actor)throws SQLException {
		ArrayList<Role> roles = new ArrayList<Role> ();
		
		StringBuffer sql = new StringBuffer (1024);
		sql.append("select * from imdb.Principals ");
		sql.append("where personID = '");
		sql.append(actor.getPersonID());
		sql.append("'");
		try (Connection conn = ConnectionFactory.getConnection()){	
			java.sql.Statement statement = conn.createStatement();
			ResultSet results = statement.executeQuery(sql.toString());
			while (results.next()) {
				
				String role = results.getString("Role");
				String movieID = results.getString("titleID");
				String personID = results.getString("personID");
				String category = results.getString("Category");
				int creditNumber = results.getInt("CreditNumber");
				
				Role rol = new Role (
					personID, movieID, role, category, creditNumber
				);
				roles.add(rol);
			}
		}
		return roles;
	}
	
	/**
	 * Finds the cast of a given movie
	 * @param movie the movie to search for
	 * @return a List of Role objects representing the roles in the
	 * specified movie
	 */
	public List<Role> findRolesInMovie (Movie movie) throws SQLException {
		ArrayList<Role> roles = new ArrayList<Role> ();
//		if (movie.getMovieID() == sleepless.getMovieID()) {
//			roles.add (hanksSleepless);
//			roles.add (ryanSleepless);
//		}
//		
//		if (movie.getMovieID() == mail.getMovieID()) {
//			roles.add (hanksMail);
//			roles.add (ryanMail);
//		}
//		
		return roles;
	}
	
	/**
	 * Finds a given actor based on Person ID
	 * @param personID the ID to search for
	 * @return the actor with the given ID, or null if none can be found
	 */
	public Actor findActorByID (String personID)throws SQLException {
		
		StringBuffer sql = new StringBuffer (1024);
		sql.append("select * from imdb.people ");
		sql.append("where personID = '");
		sql.append(personID);
		sql.append("'");
		try (Connection conn = ConnectionFactory.getConnection()){	
			java.sql.Statement statement = conn.createStatement();
			ResultSet results = statement.executeQuery(sql.toString());
			if (results.next()) {
		
				 personID = results.getString("PersonID").trim();
				String firstName = results.getString("FirstName");
			String middleName = results.getString("MiddleName");
				String lastName = results.getString("LastName");
			String suffix = results.getString("Suffix");
			String fullName = results.getString("FullName");
				int birthYear = results.getInt("BirthYear");
				int deathYear = results.getInt("DeathYear");
			Actor actor = new Actor(
			personID, firstName, middleName, lastName, suffix, fullName,
			birthYear, deathYear
			);
			return  actor;
		} 
			}
		
		return null;
	}
	
	/**
	 * Finds a given movie based on Title ID
	 * @param titleID the ID to search for
	 * @return the movie with the given ID, or null if none can be found
	 * @throws SQLException 
	 */
	public Movie findMovieByID (String titleID) throws SQLException {
		StringBuffer sql = new StringBuffer (1024);
		sql.append("select * from imdb.Movies ");
		sql.append("where titleID = '");
		sql.append(titleID);
		sql.append("'");
		try (Connection conn = ConnectionFactory.getConnection()){	
			java.sql.Statement statement = conn.createStatement();
			ResultSet results = statement.executeQuery(sql.toString());
			if (results.next()) {
				 titleID = results.getString("TitleID").trim();
				String movieTitle = results.getString("Title").trim();
				int	year = results.getInt("YearReleased");
				int	runningTimeValue = results.getInt("RunningTimeInMinutes");
				String primaryGenre = results.getString("PrimaryGenre");
				String director = results.getString("PersonID");

				Movie movie = new Movie(
				titleID, movieTitle, year, runningTimeValue);
				
				return movie;

				}
			}
		return null;
	}
	
	public List<String> findGenres ()throws SQLException {
		StringBuffer sql = new StringBuffer (1024);
		sql.append("select * from imdb.Genres ");
		
		ArrayList<String> genres = new ArrayList<String> ();
		try (Connection conn = ConnectionFactory.getConnection()){	
			java.sql.Statement statement = conn.createStatement();
			ResultSet results = statement.executeQuery(sql.toString());
			while (results.next()) {
				// Process one row of results
				String genre = results.getString("Genre");
				
				genres.add(genre);
				}
			}
		return genres;
	}
	
	/**
	 * Retrieves the list of MPAA ratings from the database.
	 * Only those Ratings that
	 * <ul> 
	 *   <li><strong>don't start with TV</strong></li>
	 *   <li>aren't the strings	<em>Unrated</em> and <em>Not Rated</em></li>
	 *   <li>have <strong>at least 1000 movies</strong> that have been made since 1970</li>
	 * </ul>
	 * should be included in the list.
	 * <p>
	 * The list should be sorted by the SortOrder field found in the MPAARatings
	 * table.
	 * </p>
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results 
	 */
	public List<String> findMPAARatings() throws SQLException {
		ArrayList<String> ratings = new ArrayList<String>();
		StringBuffer sql = new StringBuffer (1024);
		sql.append("select distinct MPAARating, count(titleId) as movieCount, sortOrder from imdb.Movies ");
		sql.append("join imdb.MPAARatings on movies.mpaaRating = mpaaRatings.Rating");
		sql.append(" where  MPAARating not like 'TV%' and MPAARating not in ('Unrated', 'Not Rated') and yearReleased >1969 ");
		sql.append("group by MPAARating, sortOrder having count(titleId) > 1000 order by sortOrder");
		try (Connection conn = ConnectionFactory.getConnection()){	
			java.sql.Statement statement = conn.createStatement();
			ResultSet results = statement.executeQuery(sql.toString());
			while (results.next()) {
				// Process one row of results
				String rating = results.getString("MPAARating");
				ratings.add(rating);
				}
			}	
		return ratings;
	}
	
	/**
	 * Retrieves the distinct list of Categories found in the Principals table.
	 * The list should be sorted so that the Categories which appear most frequently 
	 * are displayed first.   
	 * @throws SQLException if an exception occurs connecting to the DB,
	 * or in processing the results
	 */
	public List<String> findCategories() throws SQLException {
		ArrayList<String> categories = new ArrayList<String>();
		StringBuffer sql = new StringBuffer (1024);
		sql.append("select distinct category, count(People.personID) as count ");
		sql.append("from imdb.Principals join imdb.People on people.personId = principals.PersonID group by category order by COUNT desc");
		try (Connection conn = ConnectionFactory.getConnection()){	
			java.sql.Statement statement = conn.createStatement();
			ResultSet results = statement.executeQuery(sql.toString());
			while (results.next()) {
				// Process one row of results
				String category = results.getString("category");
				categories.add(category);
				}
			
			}
		return categories;
	}

	/**
	 * Retrieves a list consisting of consecutive years between the <strong>earliest</strong>
	 * and <strong>latest</strong> years for which there are movies in the database. 
	 * @return an ArrayList of integers representing the years for which movies have been made,
	 * in descending order by year
	 * @throws SQLException if an error occurs connecting to the database or processing the 
	 * rows that it is returning
	 */
	public List<Integer> getMovieYears() throws SQLException {
		List<Integer> possibleYears = new ArrayList<Integer>();
		StringBuffer sql = new StringBuffer (1024);
		sql.append("select distinct yearReleased from imdb.Movies order by yearReleased desc");
		try (Connection conn = ConnectionFactory.getConnection()){	
			java.sql.Statement statement = conn.createStatement();
			ResultSet results = statement.executeQuery(sql.toString());
			while (results.next()) {
				// Process one row of results
				Integer year = results.getInt("yearReleased");
				possibleYears.add(year);
				}
			}
		return possibleYears;
	}
}
