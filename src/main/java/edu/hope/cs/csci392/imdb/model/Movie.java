package edu.hope.cs.csci392.imdb.model;

import java.util.List;
import java.util.ArrayList;

public class Movie {
	private String titleID;
	private String title;
	private int yearReleased;
	private int runningTimeInMinutes;
	private String primaryGenre;
	private String MPAARating;
	private float imdbRating;
	private int numberOfRatings;
	private List<String> genres;

	public Movie(String titleID, String title, int yearReleased, int lengthInMinutes, List<String> genres,
			String MPAARating, float imdbRating, int numberOfRatings) {
		this.titleID = titleID;
		this.title = title;
		this.yearReleased = yearReleased;
		this.runningTimeInMinutes = lengthInMinutes;
		this.genres = new ArrayList<String>();
		this.MPAARating = MPAARating;
		this.imdbRating = imdbRating;
		this.numberOfRatings = numberOfRatings;

	}

	public Movie(String titleID2, String movieTitle, int year, int runningTimeValue) {
		this.titleID = titleID2;
		this.title = movieTitle;
		this.yearReleased = year;
		this.runningTimeInMinutes = runningTimeValue;
	}

	public String getMovieID() {
		return titleID;
	}

	public String getTitle() {
		return title;
	}

	public int getYearReleased() {
		return yearReleased;
	}

	public int getLengthInMinutes() {
		return runningTimeInMinutes;
	}

	public String getPrimaryGenre() {
		return primaryGenre;
	}

	public String getMPAARating() {
		return MPAARating;
	}

	public float getImdbRating() {
		return imdbRating;
	}

	public int getNumberOfRatings() {
		return numberOfRatings;
	}

	public List<String> getGenres() {
		return genres;
	}

	public void addGenre(String genre) {
		this.genres.add(genre);
	}
}